/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AbstractComponent } from '../common/component/abstract.component';
import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { GridComponent } from '../common/component/grid/grid.component';
import { header, SlickGridHeader } from '../common/component/grid/grid.header';
import { GridOption } from '../common/component/grid/grid.option';
import { ActivatedRoute } from '@angular/router';
import { WorkbenchService } from './service/workbench.service';
import { QueryEditor, Workbench } from '../domain/workbench/workbench';
import { Alert } from '../common/util/alert.util';
import { DetailWorkbenchTable } from './component/detail-workbench/detail-workbench-table/detail-workbench-table';
import { CommonConstant } from '../common/constant/common.constant';
import { DeleteModalComponent } from '../common/component/modal/delete/delete.component';
import { Modal } from '../common/domain/modal';
import { UserDetail } from '../domain/common/abstract-history-entity';
import { StringUtil } from '../common/util/string.util';
import { CookieConstant } from '../common/constant/cookie.constant';
import { isNullOrUndefined, isUndefined } from 'util';
import { LoadingComponent } from '../common/component/loading/loading.component';
import { DatasourceService } from '../datasource/service/datasource.service';
import { PageWidget } from '../domain/dashboard/widget/page-widget';
import { Dashboard, BoardDataSource, BoardConfiguration } from '../domain/dashboard/dashboard';
import { BIType, ConnectionType, Datasource, Field, LogicalType } from '../domain/datasource/datasource';
import { Workbook } from '../domain/workbook/workbook';
import { DataconnectionService } from '../dataconnection/service/dataconnection.service';
import { CommonUtil } from '../common/util/common.util';
import * as _ from 'lodash';
import { DetailWorkbenchSchemaBrowserComponent } from './component/detail-workbench/detail-workbench-schema-browser/detail-workbench-schema-browser.component';
import { SYSTEM_PERMISSION } from '../common/permission/permission';
import { PermissionChecker, Workspace } from '../domain/workspace/workspace';
import { WorkspaceService } from '../workspace/service/workspace.service';
import { CodemirrorComponent } from './component/editor-workbench/codemirror.component';
import { Dataconnection } from '../domain/dataconnection/dataconnection';

@Component({
  selector: 'app-workbench',
  templateUrl: './workbench.component.html',
  // host: {
  //   '(document:keyup)': 'onKeyUpEventHandler($event)',
  // }
})
export class WorkbenchComponent extends AbstractComponent implements OnInit, OnDestroy, AfterViewInit {

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Private Variables
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  // 에디터 행수
  private MAX_LINES: number = 20;

  // 그리드
  @ViewChild('main')
  private gridComponent: GridComponent;

  @ViewChild('gridWrapElement')
  private gridWrapElement: ElementRef;

  @ViewChild(DeleteModalComponent)
  private deleteModalComponent: DeleteModalComponent;

  // 에디터 관련 변수
  @ViewChild(CodemirrorComponent)
  private editor: CodemirrorComponent;

  // table 목록 관련 변수
  @ViewChild(DetailWorkbenchTable)
  private detailWorkbenchTable: DetailWorkbenchTable;

  @ViewChild(LoadingComponent)
  private loadingBar: LoadingComponent;

  // 탭 번호
  private tabNum: number = 0;

  // 선택된 탭 번호
  private selectedTabNum: number = 0;

  // websocket sbuscription
  private websocketId: string;

  // websocket id
  private webSocketLoginId: string = '';

  // websocket pw
  private webSocketLoginPw: string = '';

  // 스키마 브라우저
  @ViewChild(DetailWorkbenchSchemaBrowserComponent)
  private schemaBrowserComponent: DetailWorkbenchSchemaBrowserComponent;

  // 에디터 리스트 tabs
  @ViewChild('editorListTabs')
  private _editorListTabs: ElementRef;
  // 에디터 리스트 최대 값
  @ViewChild('editorListMax')
  private _editorListMax: ElementRef;
  // 에디터 리스트 사이즈 버튼
  @ViewChild('editorListSizeBtn')
  private _editorListSizeBtn: ElementRef;

  // 에디터 결과 리스트 tabs
  @ViewChild('editorResultListTabs')
  private _editorResultListTabs: ElementRef;
  // 에디터 결과 리스트 최대 값
  @ViewChild('editorResultListMax')
  private _editorResultListMax: ElementRef;

  @ViewChild('questionLayout')
  private _questionLayout: ElementRef;

  @ViewChild('questionWrap')
  private _questionWrap: ElementRef;

  // request reconnect count
  private _executeSqlReconnectCnt: number = 0;
  private _checkQueryStatusReconnectCnt: number = 0;

  private _subscription: any;

  private _resizeTimer: any;

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Variables
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  public isWaterfallProc:boolean = false;

  // editor 리스트 관리객체
  public editorListObj = new EditorList();

  // editor 결과 리스트 관리객체
  public editorResultListObj = new EditorList();

  // 워크 벤치 아이디
  public workbenchId: string;

  // 워크벤치 데이터
  public workbench: Workbench = new Workbench();

  // 워크벤치 데이터
  public workbenchTemp: Workbench = new Workbench();

  // 오른쪽 상단 워크벤치 메타정보 show/hide
  public isWorkbenchOptionShow: boolean = false;

  public options: any = {
    maxLines: this.MAX_LINES,
    printMargin: false,
    setAutoScrollEditorIntoView: true,
    enableBasicAutocompletion: true,
    enableSnippets: true,
    enableLiveAutocompletion: true
  };

  // result time
  public resultTime: string = '';

  // 선택된 텍스트
  public text: string = '';

  // tab list 이름만 모아뒀다
  public textList: any[] = []; // { name: '쿼리' + this.tabNum, query: '', selected: true }

  // 전체 데이터 탭 목록
  public totalResultTabList: ResultTab[] = [];

  public selectedEditorId: string;    // 선택된 에디터 아이디
  public executeEditorId: string;     // 현재 쿼리가 실행중인 에디터 아이디
  public runningResultTabId: string;  // 현재 실행 대상 탭 아이디

  // 왼쪽 메뉴 show/hide
  public isLeftMenuOpen: boolean = true;

  // dataconnection 정보 패널 Show 여부
  public isDataConnectionInfoShow: boolean = false;

  // 첫번쨰 : 글로벌 변수 선언 show/hide
  public isGlobalVariableMenuShow: boolean = false;

  // 두번째 : 쿼리히스토리 show/hide
  public isQueryHistoryMenuShow: boolean = false;

  // 세번쨰 : 네비게이션 show/hide
  public isNavigationMenuShow: boolean = false;

  public isQueryEditorFull: boolean = false;

  // 결과창 검색어
  public searchText: string = '';

  // 로그인 창 활성화 여부
  public loginLayerShow: boolean = false;

  // 워크벤치 이름 수정 모드
  public isWorkbenchNameEditMode: boolean = false;

  // 워크벤치 설명 수정 모드
  public isWorkbenchDescEditMode: boolean = false;

  // database component에 넘기기 위함 param
  public databaseParam: any;

  // table component에 넘기기 위한 paramm
  public tableParam: any;

  // 탭클릭시 보여지는 탭
  public tabLayer: boolean = false;

  public tabLayerX: string = '';

  public tabLayerY: string = '';

  public allQuery: String = 'ALL';

  // 워크벤치 datasource 저장하기
  public mode: string = '';

  // 데이터 소스 저장에 넘길 데이터.
  public setDatasource: any = {};

  // 페이지 컴포넌트 보이기 여부
  public isShowPage: boolean = false;

  // 페이지 객체에 넘기줄 겍채
  public selectedPageWidget: PageWidget;

  // 페이지 엔진 이름
  public pageEngineName: string = '';

  // 인증 방식
  public authenticationType: string = '';

  // 인터벌 객체
  public intervalDownload: any;

  // container for workbench name&desc -> edit
  public workbenchName: string;
  public workbenchDesc: string;

  public mainViewShow: boolean = true;

  public csvDownloadLayer: boolean = false;

  // 데이터 메니저 여부
  public isDataManager: boolean = false;

  // 쿼리조회후 가져올 갯 수
  public queryResultNumber: number = 1000;

  @ViewChild('wbName')
  private wbName: ElementRef;
  @ViewChild('wbDesc')
  private wbDesc: ElementRef;

  // event close
  public closeEvent: any;

  public config = {
    mode: 'text/x-hive',
    indentWithTabs: true,
    lineNumbers: true,
    matchBrackets: true,
    autofocus: true,
    indentUnit: 4,
    smartIndent: false,
    showSearchButton: true,
    extraKeys: {
      'Ctrl-Space': 'autocomplete',
      'Ctrl-/': 'toggleComment',
      'Shift-Tab': 'indentLess',
      'Tab': 'indentMore',
      'Shift-Ctrl-Space': 'autocomplete',
      'Cmd-Alt-Space': 'autocomplete'
    },
    hintOptions: {
      tables: {}
    }
  };
  //H2, HIVE, ORACLE, TIBERO, MYSQL, MSSQL, PRESTO, FILE, POSTGRESQL, GENERAL;
  public mimeType: string = 'HIVE';

  // 수정 권한을 가진 사용자 여부
  public isChangeAuthUser: boolean = false;

  // 단축키 show flag
  public shortcutsFl: boolean = false;

  // 선택된 Grid 탭 번호
  public selectedGridTabNum: number = 0;

  // 화면 로딩 완료
  public isHiveLog: boolean = false;

  // 쿼리 실행 취소 여부
  public isCancelQuery: boolean = false;

  // 현재 실행 쿼리
  public runningQueryArr: string [] = [];

  // 현재 실행 query Editor
  public runningQueryEditor: any = {};

  // 현재 실행 query done Index
  public runningQueryDoneIndex: number = -1;

  // editor selected tab number
  public tempEditorSelectedTabNum: number = 0;

  // hive log 취소중
  public hiveLogCanceling: boolean = false;

  // hive query 실행 중
  public isHiveQueryExecute: boolean = false;

  public isRunningCancel:boolean = false;    // 취소 작업 중인지...

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Constructor
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  // 생성자
  constructor(private workspaceService: WorkspaceService,
              protected activatedRoute: ActivatedRoute,
              protected workbenchService: WorkbenchService,
              protected connectionService: DataconnectionService,
              protected datasourceService: DatasourceService,
              protected element: ElementRef,
              protected injector: Injector) {
    super(element, injector);
  }

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Override Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /**
   * 컴포넌트 초기 실행
   */
  public ngOnInit() {
    super.ngOnInit();

    if (this.cookieService.get(CookieConstant.KEY.LOGIN_TOKEN) === '') {
      this.router.navigate(['/user/login']);
    }
    this.workbench.modifiedBy = new UserDetail();
    this.workbench.createdBy = new UserDetail();

    // Router에서 파라미터 전달 받기
    this.activatedRoute.params.subscribe((params) => {
      this.workbenchId = params['id'];
    });

  }

  /**
   * 화면 초기화
   */
  public ngAfterViewInit() {
    super.ngAfterViewInit();

    // Send statistics data
    this.sendViewActivityStream(this.workbenchId, 'WORKBENCH');

    // 초기 데이터 조회 - 화면이 표시되기 전에 로딩을 호출하여 표시되지 않는 문제로 지연 코드 추가
    setTimeout(() => {
      this.loadingBar.hide(); // 초기에 표시되는 문제로 숨김
      this.loadingShow();
      this._loadInitData(() => {
        this.onEndedResizing();
        this.webSocketCheck(() => this.loadingHide());
      });
    }, 500);
  } // function - ngAfterViewInit

  /**
   * 웹소켓 체크
   * @param {Function} callback
   */
  public webSocketCheck(callback?: Function) {
    this.checkAndConnectWebSocket(true).then(() => {
      try {
        this.createWebSocket(callback);
      } catch (e) {
        console.log(e);
      }
      this.websocketId = CommonConstant.websocketId;
      WorkbenchService.websocketId = CommonConstant.websocketId;
    });
  } // function - webSocketCheck

  public ngOnDestroy() {

    // Destory
    super.ngOnDestroy();
    // this.webSocketCheck(() => {});
    (this._subscription) && (CommonConstant.stomp.unsubscribe(this._subscription));     // Socket 응답 해제

    // (this.timer) && (clearInterval(this.timer));

    // save info
    if (this.cookieService.get(CookieConstant.KEY.LOGIN_TOKEN) !== '') {
      const headers: any = {
        'X-AUTH-TOKEN': this.cookieService.get(CookieConstant.KEY.LOGIN_TOKEN)
      };
      CommonConstant.stomp.send('/message/workbench/' + this.workbenchId + '/dataconnections/' + this.workbench.dataConnection.id + '/disconnect', '', headers);

      // 이전에 선택된 Query tab 저장
      if (!isUndefined(this.selectedEditorId) && !StringUtil.isEmpty(this.getLocalStorageQuery(this.selectedEditorId))) {
        const queryEditor: QueryEditor = new QueryEditor();
        queryEditor.editorId = this.selectedEditorId;
        queryEditor.name = this.textList[this.selectedTabNum]['name'];
        queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
        // queryEditor.order = this.tabNum;
        queryEditor.order = this.textList[this.selectedTabNum].order;
        // queryEditor.query = this.getSelectedTabText();
        queryEditor.query = this.getLocalStorageQuery(this.selectedEditorId);

        this.workbenchService.updateQueryEditor(queryEditor)
          .then(() => {
            this.loadingHide();
            // 로컬 스토리지에 저장된 쿼리 제거
            this.removeLocalStorage(this.selectedEditorId);
          })
          .catch(() => {
            this.loadingHide();
          });
        // 저장 종료
      }
    }
  }

  /**
   * 윈도우 리사이즈 이벤트 처리
   */
  @HostListener('window:resize', ['$event'])
  protected onResize() {
    clearTimeout(this._resizeTimer);
    this._resizeTimer = setTimeout(() => {
      this.onEndedResizing();
    }, 500);
  } // function - onResize

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /**
   * local storage 에 쿼리 저장
   * @param {string} value
   * @param {string} editorId
   */
  public saveLocalStorage(value: string, editorId: string): void {
    this.useUnloadConfirm = true;
    localStorage.setItem('workbench' + this.workbenchId + editorId, value);
  }

  /**
   * local storage 에 저장된 쿼리 제거
   * @param {string} editorId
   */
  public removeLocalStorage(editorId: string): void {
    this.useUnloadConfirm = false;
    localStorage.removeItem('workbench' + this.workbenchId + editorId);
  }

  /**
   * local storage 에 저장된 쿼리 불러오기
   * @param {string} editorId
   * @returns {string}
   */
  public getLocalStorageQuery(editorId: string) {
    return localStorage.getItem('workbench' + this.workbenchId + editorId);
  }

  /**
   * local storage 에 기본정보 저장
   */
  public saveLocalStorageGeneral(): void {
    const saveObj: any = {};
    saveObj.tabId = this.selectedTabNum;
    saveObj.schema = this.workbench.dataConnection.database;
    localStorage.setItem('workbench-general-' + this.workbenchId, JSON.stringify(saveObj));
  }

  public saveLocalStorageGeneralSchema(): void {
    const saveObj: any = {};
    const generalConnection: any = this.getLocalStorageGeneral();
    if (generalConnection !== null) {
      if (!isUndefined(generalConnection.tabId)) {
        this.selectedTabNum = generalConnection.tabId;
        saveObj.tabId = this.selectedTabNum;
      }
      if (!isUndefined(generalConnection.schema)) {
        this.workbench.dataConnection.database = generalConnection.schema;
        saveObj.schema = this.workbench.dataConnection.database;
      }
      localStorage.setItem('workbench-general-' + this.workbenchId, JSON.stringify(saveObj));
    }
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * local storage 에 저장된 기본정보 제거
   */
  public removeLocalGeneral(): void {
    localStorage.removeItem('workbench-general-' + this.workbenchId);
  }

  /**
   * local storage 에 저장된 기본정보 불러오기
   * @returns {string}
   */
  public getLocalStorageGeneral() {
    return JSON.parse(localStorage.getItem('workbench-general-' + this.workbenchId));
  }

  /**
   * 로그인 완료
   * @param param
   */
  public loginComplete(param) {
    // this.workbench = this.workbenchTemp;
    this.websocketId = CommonConstant.websocketId;
    this.webSocketLoginId = param.id;
    this.webSocketLoginPw = param.pw;
    //
    WorkbenchService.websocketId = CommonConstant.websocketId;
    WorkbenchService.webSocketLoginId = param.id;
    WorkbenchService.webSocketLoginPw = param.pw;
    this.readQuery(this.workbenchTemp, this.workbenchTemp.queryEditors);

    //TODO The connection has not been established error
    try {
      this.webSocketCheck(() => {
      });
    } catch (e) {
      console.log(e);
    }
  } // function - loginComplete

  /**
   * 새로운 에디터 만들기
   * @param {string} text
   * @param {boolean} selectedParam
   * @param {boolean} increase
   */
  public createNewEditor(text: string = '', selectedParam: boolean = true, increase: boolean = false) {
    // 탭번호 증가
    if (increase === true) {
      this.tabNum = Number(this.textList.length);
    }

    const tabPrefix: string = this.translateService.instant('msg.bench.ui.tab-prefix');
    const prefixReg: RegExp = new RegExp(tabPrefix);
    const numRegExp: RegExp = /^[0-9]+$/;
    const numList: number[]
      = this.textList
      .map(item => item.name.replace(prefixReg, '').trim())
      .filter(item => numRegExp.test(item))
      .map(item => Number(item));
    const lastTabNum: number = ( numList && 0 < numList.length ) ? _.max(numList) : 0;

    const queryEditor: QueryEditor = new QueryEditor();
    queryEditor.name = tabPrefix + ' ' + (lastTabNum + 1);
    queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
    queryEditor.order = this.tabNum;
    queryEditor.query = '';

    this.loadingShow();
    this.workbenchService.createQueryEditor(queryEditor)
      .then((data) => {
        this.loadingHide();
        // 탭 추가
        this.textList.push({
          selected: selectedParam,
          name: queryEditor.name,
          query: text === '' ? '' : text,
          editorId: data.id,
          editorMode: false
        });
        // 슬라이드 아이콘 show hide
        this._calculateEditorSlideBtn();
        // 로컬 스토리지에 저장하기 위해
        this.selectedTabNum = this.textList.length - 1;
        this.saveLocalStorageGeneral();
        // 끝
        // 탭 변경
        this.tabChangeHandler(this.textList.length - 1, false);
      })
      .catch((error) => {
        this.loadingHide();
        if (!isUndefined(error.details)) {
          Alert.error(error.details);
        } else {
          Alert.error(error);
        }
      });

  } // function - createNewEditor

  /**
   * 탭 닫기
   * @param tabNum
   */
  public closeEditorTab(tabNum) {
    // 탭이 하나라면
    if (this.textList.length === 1) {
      Alert.warning(this.translateService.instant('msg.bench.alert.close.editortab.fail'));
      return;
    }

    // 해당 tab 삭제.
    this.workbenchService.deleteQueryEditor(this.textList[tabNum]['editorId'])
      .then(() => {
        // 삭제 성공.
      })
      .catch((error) => {
        if (!isUndefined(error.details)) {
          Alert.error(error.details);
        } else {
          Alert.error(error);
        }
      });

    // 탭 삭제
    this.textList.splice(tabNum, 1);
    // 슬라이드 아이콘 show hide
    this._calculateEditorSlideBtn(true);

    // 탭 번호 낮추기
    if (this.textList.length === 1) {
      this.selectedTabNum = 0;
    } else {
      if (this.textList.length <= tabNum) {
        this.selectedTabNum = this.textList.length - 1;
      } else {
        this.selectedTabNum = tabNum;
      }
    }


    // 탭이 하나라도 존재한다면
    if (this.textList.length > 0) {
      // 첫번재 탭을 선택상태로 변경
      // this.tabChangeHandler(this.selectedTabNum, true);
      this._saveAllQueryEditor();
      // 탭 첫번째로 초기화
      for (let index: number = 0; index < this.textList.length; index = index + 1) {
        // 선택된 탭이면
        if (index === 0) {
          this.textList[index]['selected'] = true;
          this.selectedTabNum = index;
          this.selectedEditorId = this.textList[index]['editorId'];
          this.setSelectedTabText(this.textList[index]['query']);
          this.selectedTabNum = index;
        } else {
          // 선택되지 않은 탭이면
          this.textList[index]['selected'] = false;
        }
      }

      // 그리드 재정렬
      const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
      if (0 < currentEditorResultTabs.length) {
        this.drawGridData();
      }
    }
    this.saveLocalStorageGeneral();
  } // function - closeEditorTab

  // 편집 모드 종료
  public tabLayerBlur(item, $event) {
    if (item['editorMode']) {
      this.tabLayerEnter($event);
    }
  }

  /**
   * end editing for editor tab name
   * @param $event
   */
  public tabLayerEnter($event) {
    if ($event.target.value == '') {
      Alert.warning(this.translateService.instant('msg.bench.ui.query.tab.title'));
    } else {
      this.textList[this.selectedTabNum]['editorMode'] = false;
      this.textList[this.selectedTabNum]['name'] = $event.target.value;

      // 변경된 쿼리 이름 저장
      // 저장할 객체
      const queryEditor: QueryEditor = new QueryEditor();
      queryEditor.editorId = this.textList[this.selectedTabNum].editorId;
      queryEditor.name = this.textList[this.selectedTabNum].name;
      queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
      queryEditor.order = this.textList[this.selectedTabNum].order;
      queryEditor.query = this.textList[this.selectedTabNum].query;
      this.workbenchService.updateQueryEditor(queryEditor)
        .then(() => {
          // 로컬 스토리지에서 쿼리 삭제
          this.removeLocalStorage(queryEditor.editorId);
          // 그리드 이름 변경
          this.totalResultTabList.forEach((item, index) => {
            if (item.editorId === this.selectedEditorId) {
              item.name = this._genResultTabName(this.textList[this.selectedTabNum].name, 'RESULT', (index + 1));
            }
          });
        })
        .catch((error) => {
          this.loadingHide();
          if (!isUndefined(error.details)) {
            Alert.error(error.details);
          } else {
            Alert.error(error);
          }
        });
    }
  } // function - tabLayerEnter

  /**
   * close data tap event handler
   * @param tabId
   */
  public closeResultTab(tabId: string) {

    let currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();

    if (currentEditorResultTabs.length === 1) {
      // Alert.warning('결과 탭을 닫을수 없습니다.');
      this._removeResultTab(tabId);
      this.selectedGridTabNum = 0;
      this.gridSearchClear();
      return;
    }

    // 결과창 검색어 초기화
    this.gridSearchClear();

    let removeIdx: number = currentEditorResultTabs.findIndex(item => item.id === tabId);

    // 탭 삭제
    this._removeResultTab(tabId);

    // show index 가 0이라면 icon flag 재계산
    if (this.editorResultListObj.index === 0) {
      // 변경이 다 일어났을 때
      this.changeDetect.detectChanges();
      this.editorResultListObj.showBtnFl = this._isEditorResultMaxWidth();
    }

    currentEditorResultTabs = this._getCurrentEditorResultTabs();
    if (currentEditorResultTabs.length > 0) {
      let showTabInfo: ResultTab = currentEditorResultTabs[removeIdx - 1];
      this.changeResultTabHandler(showTabInfo.id);
    }
  } // function - closeResultTab

  /**
   * change editor tab
   * @param {number} selectedTabNum
   * @param {boolean} deleteFlag
   * @param selectedItem
   */
  public tabChangeHandler(selectedTabNum: number, deleteFlag: boolean = false, selectedItem?: any): void {

    // 이전에 선택된 Query tab 저장
    if (!isUndefined(this.selectedEditorId) && deleteFlag === false) {
      // 로컬 스토리지에 선택된 tab 순번과 schema 저장
      const queryEditor: QueryEditor = new QueryEditor();
      const selectedTabIndex = _.findIndex(this.textList, { selected: true });
      queryEditor.editorId = this.selectedEditorId;
      // queryEditor.name = this.textList[this.selectedTabNum]['name'];
      queryEditor.name = this.textList[selectedTabIndex]['name'];
      queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
      // queryEditor.order = this.textList[selectedTabIndex]['queryTabNum'];//this.tabNum;
      queryEditor.order = selectedTabIndex;
      // queryEditor.query = this.getSelectedTabText();
      queryEditor.query = this.textList[selectedTabIndex]['query'];

      this.workbenchService.updateQueryEditor(queryEditor)
        .then(() => {
          // this.loadingHide();
          // 로컬 스토리지에서 쿼리 삭제
          this.removeLocalStorage(queryEditor.editorId);
        })
        .catch((error) => {
          // this.loadingHide();
          if (!isUndefined(error.details)) {
            Alert.error(error.details);
          } else {
            Alert.error(error);
          }
        });
      // 저장 종료
    }

    if (selectedItem && selectedItem['selected'] && selectedItem['editorMode']) {
      return;
    }

    for (let index: number = 0; index < this.textList.length; index = index + 1) {
      // 선택된 탭이면
      if (index === selectedTabNum) {
        this.textList[index]['selected'] = true;
        this.selectedTabNum = index;
        this.selectedEditorId = this.textList[this.selectedTabNum]['editorId'];
        this.setSelectedTabText(this.textList[this.selectedTabNum]['query']);
        this.selectedTabNum = index;
      } else {
        // 선택되지 않은 탭이면
        this.textList[index]['selected'] = false;
      }
    }

    this.safelyDetectChanges(); // 변경값 반영

    const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();

    // 에디터 결과 슬라이드 버튼 계산
    this._calculateEditorResultSlideBtn(true);


    // 그리드 있을때만
    if (currentEditorResultTabs.length > 0) {

      // 로그 숨김 처리
      currentEditorResultTabs.forEach(item => item.showLog = false);

      // 선택된 결과 탭 정보 조회
      let resultTab = currentEditorResultTabs.find(tabItem => tabItem.selected);
      if (!resultTab) {
        resultTab = currentEditorResultTabs[0];
        resultTab.selected = true;
      }

      // 결과 탭 변경
      this.changeResultTabHandler(resultTab.id);

    }
    if (selectedItem) {
      this.saveLocalStorageGeneral();
    }
  } // function - tabChangeHandler

  /**
   * Change result tab
   * @param {string} selectedTabId
   */
  public changeResultTabHandler(selectedTabId: string) {

    let selectedTab: ResultTab = null;
    const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
    currentEditorResultTabs.forEach((tabItem: ResultTab, idx: number) => {
      // 선택된 탭이면
      if (tabItem.id === selectedTabId) {

        tabItem.selected = true;
        this.selectedGridTabNum = idx;

        if (isNullOrUndefined(tabItem.data)) {
          tabItem.showLog = true;
        } else {
          tabItem.showLog = false;
          // 중지된 탭의 경우 체크
          if (isNullOrUndefined(tabItem.data.data)) {
            $('.myGrid').html('<div class="ddp-text-result ddp-nodata">' + this.translateService.instant('msg.storage.ui.no.data') + '</div>');
          }
        }
        selectedTab = tabItem;

      } else {
        // 선택되지 않은 탭이면
        tabItem.selected = false;
      }
    });

    (selectedTab.showLog) || (this.drawGridData());

    this.safelyDetectChanges();

  } // function - changeResultTabHandler

  /**
   * editor key input event handler
   * @param event
   * @return {boolean}
   */
  public editorKeyEvent(event) {
    // 쿼리 실행.
    if (event.ctrlKey && event.keyCode === 13) {
      this.setExecuteSql('SELECTED');
    }

    if (event.ctrlKey && event.keyCode === 81) {
      this.clearSql();
    }

    if (event.ctrlKey && event.keyCode === 190) {
      this.setSqlFormatter();
    }

    if (event.altKey && event.keyCode === 13) {
      this.setExecuteSql('ALL');
    }

    // 현재 저장된 쿼리랑 다를때
    if (this.textList.length !== 0 && this.getLocalStorageQuery(this.selectedEditorId) !== this.getSelectedTabText()) {
      // 쿼리 저장
      this.textList[this.selectedTabNum]['query'] = this.getSelectedTabText();
      // 로컬 스토리지에 쿼리에 저장
      this.saveLocalStorage(this.getSelectedTabText(), this.textList[this.selectedTabNum]['editorId']);
    }
  } // function - editorKeyEvent

  /**
   * editor contents change event handler
   * @param {string} param
   */
  public editorTextChange(param: string) {
    this.textList[this.selectedTabNum]['query'] = param;
  } // function - editorTextChange

  /**
   * open or close left Menu
   */
  public leftMenuOpen() {
    this.isLeftMenuOpen = !this.isLeftMenuOpen;
    // 아이콘 슬라이드 버튼 계산
    this._calculateEditorSlideBtn();
    this._calculateEditorResultSlideBtn();
  } // function - leftMenuOpen

  /**
   * open or cloe data connection info layer
   */
  public dataConnectionInfoShow() {
    this.isDataConnectionInfoShow = !this.isDataConnectionInfoShow;
  } // function - dataConnectionInfoShow

  /**
   * set init database
   * @param $event
   */
  public setInitDatabase($event) {
    // database 변경
    const generalConnection: any = this.getLocalStorageGeneral();
    if (generalConnection !== null) {
      if (!isUndefined(generalConnection.tabId)) {
        this.selectedTabNum = generalConnection.tabId;
      }
      if (!isUndefined(generalConnection.schema)) {
        this.workbench.dataConnection.database = generalConnection.schema;
      } else {
        this.workbench.dataConnection.database = $event;
      }
    } else {
      this.workbench.dataConnection.database = $event;
    }

    this.tableParam = {
      dataconnection: this.workbench.dataConnection,
      webSocketId: this.websocketId
    };
    // 보고있는 schemalayer hide
    this.closeEvent = { name: 'closeSchema' };
  } // function - setInitDatabase

  /**
   * change database and dataset
   * @param $event
   */
  public setChangeDatabase($event) {
    this.workbench.dataConnection.database = $event;
    this.tableParam = {
      dataconnection: this.workbench.dataConnection,
      webSocketId: this.websocketId
    };
    this.saveLocalStorageGeneralSchema();
    // 보고있는 schemalayer hide
    this.closeEvent = { name: 'closeSchema' };
  } // function - setChangeDatabase

  /**
   * 우측 패널 구성 - 첫번쨰 : 글로벌 변수 선언
   */
  public openGlobalVariableMenu() {
    this.isGlobalVariableMenuShow = !this.isGlobalVariableMenuShow;
    this.isQueryHistoryMenuShow = false;
    this.isNavigationMenuShow = false;
    // this.isWorkbenchOptionShow = false;
  }

  /**
   * 우측 패널 구성 - 두번째 : 쿼리 히스토리 리스트
   */
  public openQueryHistoryMenu() {
    this.isQueryHistoryMenuShow = !this.isQueryHistoryMenuShow;
    this.isGlobalVariableMenuShow = false;
    this.isNavigationMenuShow = false;
    // this.isWorkbenchOptionShow = false;
  }

  /**
   * 우측 패널 구성 - 세번째 : 네비게이션
   */
  public openNavigationMenu() {
    this.isNavigationMenuShow = !this.isNavigationMenuShow;
    this.isGlobalVariableMenuShow = false;
    this.isQueryHistoryMenuShow = false;
  }

  /**
   * 우측 패널 구성 - 워크밴치 옵션 레이어 toggle
   */
  public showOption() {
    this.isWorkbenchOptionShow = !this.isWorkbenchOptionShow;
  }

  /**
   * Execute Query
   * @param {string} param
   */
  public setExecuteSql(param: string) {

    if (this.isHiveQueryExecute) {
      Alert.warning(this.translateService.instant('msg.bench.ui.query.run'));
      return;
    }

    if (this.getSelectedTabText().trim() === '') {
      Alert.warning(this.translateService.instant('msg.bench.alert.execute.query'));
      this.isHiveQueryExecute = false;
      return;
    }

    this.loadingBar.show();
    this._executeSqlReconnectCnt++; // 호출횟수 증가

    // 호출 정보 초기화
    if (this.isWaterfallProc) {
      this.isHiveQueryExecute = true;
    }
    this.isRunningCancel = false;
    this.executeEditorId = this.selectedEditorId;
    this.isCancelQuery = false;
    this.selectedGridTabNum = 0;
    this.editorResultListObj = new EditorList();
    this._clearCurrentEditorResultTabs();

    // console.info('%c >>>>>> this.executeEditorId', 'color:#FF0000', this.executeEditorId);

    this.allQuery = param;

    const queryEditor: QueryEditor = new QueryEditor();
    let runningQuery: string = '';
    {
      // 실행 후 바로 탭 이동이 되는 경우가 있으므로, 서비스 호출 전에 에디터 정보를 설정한다.
      queryEditor.name = this.textList[this.selectedTabNum]['name'];
      queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
      queryEditor.order = this.selectedTabNum;
      queryEditor.query = this.getSelectedTabText();      // 전체 쿼리 저장
      queryEditor.webSocketId = this.websocketId;
      queryEditor.editorId = this.textList[this.selectedTabNum]['editorId'];

      if (this.queryResultNumber && this.queryResultNumber !== 0) {
        queryEditor['numRows'] = this.queryResultNumber;
      } else {
        this.queryResultNumber = 1000;
        queryEditor['numRows'] = this.queryResultNumber;
      }

      // 실행 쿼리 찾기
      if (param === 'ALL') {
        runningQuery = this.getSelectedTabText();
      } else if (param === 'SELECTED') {
        if (this.getSelectedSqlTabText().trim() === '') {
          this.editor.getFocusSelection();
          if (this.getSelectedSqlTabText().trim() === '') {
            Alert.info(this.translateService.instant('msg.bench.alert.no.selected.query'));
            this.isHiveQueryExecute = false;
            this.loadingBar.hide();
            return;
          }
          runningQuery = this.getSelectedSqlTabText();
        } else {
          runningQuery = this.getSelectedSqlTabText();
        }
      }
    }

    this.workbenchService.checkConnectionStatus(this.textList[this.selectedTabNum]['editorId'], this.websocketId)
      .then((result) => {
        // 호출횟수 초기화
        this._executeSqlReconnectCnt = 0;

        if (result === 'RUNNING' || result === 'CANCELLED') {
          Alert.warning(this.translateService.instant('msg.bench.ui.query.run'));
          this.isHiveQueryExecute = false;
          this.loadingBar.hide();
          return;
        } else {

          // this.loadingBar.show();

          const tempSelectedTabNum = this.selectedTabNum;
          this.workbenchService.updateQueryEditor(queryEditor)
            .then(() => {
              // this.loadingBar.hide();
              // 로컬 스토리지에 저장된 쿼리 제거
              this.removeLocalStorage(this.selectedEditorId);

              // hive log view show
              if (this.isWaterfallProc) {

                //쿼리 실행
                // this.loadingBar.hide();

                // 전체 query data 생성
                let queryStrArr: string[]
                  = runningQuery.replace(/--.*/gmi, '').split(';').filter(item => !/^\s*$/.test(item));

                if (0 === queryStrArr.length) {
                  Alert.warning(this.translateService.instant('msg.bench.alert.execute.query'));
                  this.isHiveQueryExecute = false;
                  this.loadingBar.hide();
                  return;
                }

                // 쿼리 초기화
                this.runningQueryArr = queryStrArr;
                this.runningQueryDoneIndex = 0;
                queryEditor.query = this.runningQueryArr[0];

                // hive log cancel 여부
                this.isHiveLog = true;

                this.safelyDetectChanges();
              } else {
                queryEditor.query = runningQuery;
              }

              this.runningQueryEditor = queryEditor;

              this.runSingleQueryWithInvalidQuery(queryEditor, tempSelectedTabNum, 0);

            })
            .catch(() => {
              this.loadingBar.hide();
            });
        }
      })
      .catch((error) => {
        if (!isUndefined(error.details) && this._executeSqlReconnectCnt <= 5) {
          // Alert.error(error.details);
          this.webSocketCheck(() => {
            this.setExecuteSql(param)
          });
        } else {
          Alert.error(error);
        }
      });

  } // function - setExecuteSql

  /**
   * runSingleQueryWithInvalidQuery
   *
   * @param queryEditor
   * @param tempSelectedTabNum
   * @param selectedResultTabNum
   */
  public runSingleQueryWithInvalidQuery(queryEditor, tempSelectedTabNum, selectedResultTabNum) {

    if (this.isWaterfallProc) {
      const executeTab = this._appendResultTabByName(this.executeEditorId, 'Loading..');
      this.runningResultTabId = executeTab.id;
    }

    // console.info('%c >>>>>> Start Query - this.runningResultTabId', 'color:#0000FF', this.runningResultTabId);

    this.workbenchService.runSingleQueryWithInvalidQuery(queryEditor)
      .then((result) => {
        this.loadingBar.hide();

        // console.info('%c >>>>>> End Query - this.runningResultTabId', 'color:#0000FF', this.runningResultTabId);

        try {
          if (this.isWaterfallProc) {
            if (0 < result.length) {
              this.setHiveQueryResult(result[0]); // hive일 경우 단건 호출
            }
          } else {
            this.setQueryResult(result);
          }  // end if - hive, else
        } catch (err) {
          console.error(err);
        }
      })
      .catch(error => {

        if (this.hiveLogCanceling) {
          Alert.error(this.translateService.instant('msg.bench.alert.log.cancel.error'));
          this.loadingBar.hide();
          this.afterCancelQuery(false);
        } else {
          this.loadingBar.hide();

          if (!isUndefined(error.details)) {
            Alert.error(error.details);
          } else {
            Alert.error(error);
          }
        }

      });
  } // function - runSingleQueryWithInvalidQuery

  /**
   * 에디터 풀 사이즈처리
   */
  public resizeQueryEditor() {

    this.isQueryEditorFull = !this.isQueryEditorFull;

    this.onEndedResizing();
  } // function - resizeQueryEditor

  /**
   * 선택한 탭에 대한 SQL Clear
   */
  public clearSql() {
    this.setSelectedTabText('');
    // 쿼리 저장
    this.textList[this.selectedTabNum]['query'] = this.getSelectedTabText();
    // 로컬 스토리지에 쿼리에 저장
    this.saveLocalStorage(this.getSelectedTabText(), this.textList[this.selectedTabNum]['editorId']);
  } // function - clearSql

  /**
   * confirmDelete
   */
  public confirmDelete() {

    event.stopPropagation();

    const modal = new Modal();
    modal.name = this.translateService.instant('msg.bench.ui.wb.del');
    modal.description = this.translateService.instant('msg.bench.ui.wb.del.description');

    this.deleteModalComponent.init(modal);

  } // function - confirmDelete

  /**
   * Delete Workbench
   * @param isLoad
   */
  public deleteWorkBench(isLoad?) {
    if (isLoad) this.loadingShow();
    this.workbenchService.deleteWorkbench(this.workbenchId).then(() => {
      Alert.success(this.translateService.instant('msg.comm.alert.delete.success'));
      this.loadingHide();
      this.router.navigate(['/workspace']);
    }).catch((error) => {
      this.loadingHide();
      if (!isUndefined(error.details)) {
        Alert.error(error.details);
      } else {
        Alert.error(error);
      }
    });
  } // function - deleteWorkBench

  // 변수 추가
  public addEditorVariable(event) {
    // 선택되어 있는 탭에 텍스트가 없다면
    if (StringUtil.isEmpty(this.getSelectedTabText())) {
      // 선택된 탭 에디터에 TABLE SQL 주입
      this.setSelectedTabText(event);
    } else {
      // 에디터 포커싱 위치에 SQL 주입
      this.editor.insert(event);
    }

  }

  /**
   * update workbench name or description
   */
  public updateWorkbench() {
    if (this.workbenchName.trim() === '' || this.workbenchName === '') {
      Alert.warning(this.translateService.instant('msg.comm.ui.create.name'));
      return;
    }

    if (CommonUtil.getByte(this.workbenchName) > 150) {
      Alert.warning(this.translateService.instant('msg.alert.edit.name.len'));
      return;
    }

    if (!StringUtil.isEmpty(this.workbenchDesc) && CommonUtil.getByte(this.workbenchDesc) > 450) {
      Alert.warning(this.translateService.instant('msg.alert.edit.description.len'));
      return;
    }

    this.workbenchDesc ? this.workbenchDesc = this.workbenchDesc.trim() : null;
    this.workbenchName = this.workbenchName.trim();

    const params = {
      id: this.workbench.id,
      name: this.workbenchName,
      description: this.workbenchDesc
    };

    this.loadingShow();
    this.workbenchService.updateWorkbench(params)
      .then((workbench: Workbench) => {
        this.loadingHide();
        this.workbenchTemp = workbench;
        this.isWorkbenchNameEditMode = false;
        this.isWorkbenchDescEditMode = false;
        this.wbName.nativeElement.blur();
        this.wbDesc.nativeElement.blur();
      })
      .catch((error) => {
        this.loadingHide();
        if (!isUndefined(error.details)) {
          Alert.error(error.details);
        } else {
          Alert.error(error);
        }
      });
  } // function - updateWorkbench

  /**
   * 결과창 검색어 초기화
   */
  public gridSearchClear(): void {
    this.searchText = '';
    (this.gridComponent) && (this.gridComponent.search(this.searchText));
  } // function - gridSearchClear

  /**
   * 그리드 검색 실행
   * @param {Event} event
   */
  public gridSearch(event: Event): void {

    // 키 코드가 escape 이라면
    if (this.isKeyCodeEscape(event)) {
      // 결과창 검색어 초기화
      this.gridSearchClear();
    }

    // 그리드가 생성되어 있지 않은 경우 return;
    if (typeof this.gridComponent === 'undefined') {
      return;
    }

    // 그리드 검색 함수 호출
    this.gridComponent.search(this.searchText);
  } // function - gridSearch

  // 에디터 컴포넌트에 TABLE SQL 주입
  public sqlIntoEditorEvent(tableSql: string): void {

    // 선택되어 있는 탭에 텍스트가 없다면
    // if (StringUtil.isEmpty(this.getSelectedTabText())) {
    //   // 선택된 탭 에디터에 TABLE SQL 주입
    //   this.setSelectedTabText(tableSql);
    // } else {
    // 에디터 포커싱 위치에 SQL 주입
    this.editor.insert(tableSql);
    // 쿼리 저장
    this.textList[this.selectedTabNum]['query'] = this.getSelectedTabText();
    // 로컬 스토리지에 쿼리에 저장
    this.saveLocalStorage(this.getSelectedTabText(), this.textList[this.selectedTabNum]['editorId']);
    // }
  }

  /**
   * 패널 사이즈 변경 - 드래그 한 후 사용자가 구분 기호를 놓을 때 발생
   */
  public onEndedResizing(): void {

    this.safelyDetectChanges();

    // 에디터의 높이 값 구하기
    const editorHeight = this.getEditorComponentElementHeight();

    // 에디터 리사이징 호출
    this.editor.resize(editorHeight);

    if (typeof this.gridComponent !== 'undefined'
      && typeof this.gridComponent.dataView !== 'undefined'
      && this.gridComponent.dataView.getItems().length > 0) {

      // 그리드 높이 값 변경 함수 호출
      this.gridComponent.resize();
    }

    // 에디터 슬라이드 계산
    this._calculateEditorSlideBtn();
    this._calculateEditorResultSlideBtn();

  } // function - onEndedResizing

  // 탭 레이어 보이기
  public setTabLayer($event: Event, index: number): void {

    $event.stopImmediatePropagation();

    const offset: any = document.getElementById(`tabLayer${index}`).getBoundingClientRect();
    this.tabLayer = true;
    this.tabLayerX = `${offset.left}px`;
    this.tabLayerY = `${offset.top + 15}px`;
  }

  public setTabLayerClose() {
    this.tabLayer = false;
  }

  // form 으로 다운로드
  public downloadExcel(): void {
    this.csvDownloadLayer = true;
  }

  public downloadExcelClose(): void {
    this.csvDownloadLayer = false;
  }

  public setDownloadLocal() {
    this.csvDownloadLayer = false;
    if (typeof this.gridComponent !== 'undefined'
      && typeof this.gridComponent.dataView !== 'undefined'
      && this.gridComponent.getRows().length > 0) {
      this.loadingBar.show();
      this.gridComponent.csvDownload('result_' + Date.now().toString());
      this.loadingBar.hide();
    } else {
      this.loadingBar.hide();
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }
  }

  /**
   * download excel
   */
  public downloadServerExcel() {
    // selected dataGrid
    const dataGrid: ResultTab = this._getCurrentResultTab();
    // data grid 결과가 없을때 return
    if (isUndefined(dataGrid) || dataGrid.output === 'text') {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }
    try {
      let tempTableInfo = '';
      const that = this;

      if (!isUndefined(dataGrid.data.tempTable)) {
        tempTableInfo = dataGrid.data.tempTable;
      }
      const form = document.getElementsByTagName('form');
      const inputs = form[0].getElementsByTagName('input');
      inputs[0].value = this.cookieService.get(CookieConstant.KEY.LOGIN_TOKEN);
      inputs[1].value = this.websocketId;
      inputs[2].value = 'result_' + Date.now().toString() + '.csv';
      inputs[3].value = dataGrid.data.runQuery;
      inputs[4].value = this.workbench.dataConnection.id;
      inputs[5].value = this.workbenchId;
      inputs[6].value = tempTableInfo;
      // this.loadingShow();
      this.loadingBar.show();
      const downloadCsvForm = $('#downloadCsvForm');
      downloadCsvForm.attr('action', CommonConstant.API_CONSTANT.API_URL + `queryeditors/${this.selectedEditorId}/query/download`);
      downloadCsvForm.submit();
      this.intervalDownload = setInterval(() => that.checkQueryStatus(), 1000);
    } catch (e) {
      // 재현이 되지 않음.
      console.info('다운로드 에러' + e);
    }
  } // function - downloadServerExcel

  /**
   * checkQueryStatus
   */
  public checkQueryStatus() {
    // 호출 횟수 증가
    this._checkQueryStatusReconnectCnt++;

    this.workbenchService.checkConnectionStatus(this.selectedEditorId, this.websocketId)
      .then((result) => {
        // 호출 횟수 초기화
        this._checkQueryStatusReconnectCnt = 0;

        if (result === 'IDLE' || result === 'CANCELLED') {
          // this.loadingHide();
          this.loadingBar.hide();
          clearInterval(this.intervalDownload);
        }
      })
      .catch((error) => {
        if (error.detail && this._checkQueryStatusReconnectCnt <= 5) {
          this.webSocketCheck(this.checkQueryStatus);
        } else {
          this.loadingBar.hide();
          clearInterval(this.intervalDownload);
        }
      });
  } // function - checkQueryStatus

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Method - 슬라이드 아이콘 관련
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  public get visibleResultTabs() {
    const resultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
    return resultTabs ? resultTabs : [];
  } // function - visibleResultTabs

  public get visibleResultTab() {
    const resultTab: ResultTab = this._getCurrentResultTab();
    return resultTab ? resultTab : { log: [], showLog: false, id: '' };
  } // function - visibleResultTab

  // noinspection JSMethodCanBeStatic
  /**
   * 필터링된 탭 리스트
   * @param list
   * @param {EditorList} listObj
   */
  public getFilteringList(list: any, listObj: EditorList) {
    return list.slice(listObj.index, list.length);
  }

  // noinspection JSMethodCanBeStatic
  /**
   * list에서 index값 계산
   * @param list
   * @param item
   * @returns {number}
   */
  public findIndexInList(list: any, item: any): number {
    return _.findIndex(list, item);
  }

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Public Method - event
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /**
   * 슬라이드 prev 버튼 클릭 이벤트
   */
  public onClickPrevSlideBtn(listObj: EditorList): void {
    // index 가 0보다 크면 하나 감소
    if (listObj.index > 0) {
      listObj.index--;
      // 아이콘 슬라이드 계산
      listObj === this.editorListObj ? this._calculateEditorSlideBtn() : this._calculateEditorResultSlideBtn();
    }
  }

  /**
   * 슬라이드 next 버튼 클릭 이벤트
   */
  public onClickNextSlideBtn(listObj: EditorList): void {
    // list 가 화면에 꽉 차있고
    // index 가 list 길이보다 작다면 하나 증가
    if ((listObj === this.editorListObj ? this._isEditorMaxWidth() : this._isEditorResultMaxWidth())
      && listObj.index < listObj.list.length - 1) {
      listObj.index++;
    }
  }

  /**
   * 워크벤치 에디터 단축키 보기 클릭 이벤트
   */
  public onClickShowShortcutsBtn(): void {
    this.shortcutsFl = true;
    this._questionLayout.nativeElement.style.top = this._questionWrap.nativeElement.getBoundingClientRect().top + window.pageYOffset - document.documentElement.clientTop + 34 + 'px';
  }

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   | Private Method
   |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /**
   * 초기 데이터 조회
   * @param {Function} connectWebSocket
   * @private
   */
  private _loadInitData(connectWebSocket: Function) {

    // 워크벤치 아이디 저장
    // read Workbench With View Projection
    this.workbenchService.getWorkbench(this.workbenchId).then((data) => {
      // 워크벤치 데이터
      WorkbenchService.workbench = data;
      WorkbenchService.workbenchId = this.workbenchId;

      // 퍼미션 조회를 위한 워크스페이스 정보 조회 및 퍼미션 체커 설정
      this.workspaceService.getWorkSpace(data.workspace.id, 'forDetailView').then((workspace: Workspace) => {

        // 퍼미션 체커 정의
        const permissionChecker: PermissionChecker = new PermissionChecker(workspace);

        if (workspace.active && permissionChecker.isViewWorkbench()) {

          // 관리 유저 여부 설정
          this.isChangeAuthUser =
            (permissionChecker.isManageWorkbench() || permissionChecker.isEditWorkbench(data.createdBy.username));

          this.mimeType = data.dataConnection.implementor.toString();
          this.isWaterfallProc = ( 'HIVE' === this.mimeType );
          this.authenticationType = data.dataConnection['authenticationType'] || 'MANUAL';
          if (data.dataConnection['authenticationType'] === 'DIALOG') {
            this.loginLayerShow = true;
            this.workbenchTemp = data;
          } else {
            this.workbenchTemp = data;
            this.readQuery(this.workbenchTemp, this.workbenchTemp.queryEditors);
            this.webSocketLoginId = '';
            this.webSocketLoginPw = '';

            // connectWebSocket.call(this);
          }
          connectWebSocket.call(this);

          this.isDataManager = CommonUtil.isValidPermission(SYSTEM_PERMISSION.MANAGE_DATASOURCE);

          this.setWorkbenchName();
          this.setWorkbenchDesc();

        } else {
          // 경고창 표시
          this.openAccessDeniedConfirm();
        }

      });

    }).catch((error) => {
      if (!isUndefined(error.details)) {
        Alert.error(error.details);
      } else {
        Alert.error(error);
      }
    });
  } // function - _loadInitData


  /**
   * 커넥션이 URL 타입인지
   * @returns {boolean}
   * @private
   */
  private _isUrlType(): boolean {
    return !StringUtil.isEmpty(this.workbench.dataConnection.url);
  }

  /**
   * 에디터 슬라이드 버튼 계산
   * @param {boolean} indexInit
   * @private
   */
  private _calculateEditorSlideBtn(indexInit: boolean = false): void {
    // 에디터 리스트 객체에 list 전달
    this.editorListObj.list = this.textList;
    // 만약 delete가 일어났을 경우 index가 0으로 초기화
    if (indexInit) {
      // index가 0으로 초기화
      this.editorListObj.index = 0;
    }

    // 길이 계산은 index가 0일 경우에만 계산한다
    if (this.editorListObj.index === 0) {
      // 변경이 다 일어났을 때
      this.changeDetect.detectChanges();
      // 아이콘 버튼 show flag
      this.editorListObj.showBtnFl = this._isEditorMaxWidth();
    }
  } // function - _calculateEditorSlideBtn

  /**
   * 에디터 결과 슬라이드 버튼 계산
   * @param {boolean} indexInit
   * @private
   */
  private _calculateEditorResultSlideBtn(indexInit: boolean = false): void {
    // 에디터 리스트 객체에 list 전달
    this.editorResultListObj.list = this._getCurrentEditorResultTabs();
    // indexInit
    if (indexInit) {
      // index가 0으로 초기화
      this.editorResultListObj.index = 0;
    }

    // 길이 계산은 index가 0일 경우에만 계산한다
    if (this.editorResultListObj.index === 0) {
      // 변경이 다 일어났을 때
      this.changeDetect.detectChanges();
      // 아이콘 버튼 show flag
      this.editorResultListObj.showBtnFl = this._isEditorResultMaxWidth();
    }
  } // function - _calculateEditorResultSlideBtn

  /**
   * 에디터 width가 max인가
   * @returns {boolean}
   * @private
   */
  private _isEditorMaxWidth(): boolean {
    // 현재 생성된 list 의 길이가 최종길이-사이즈버튼 길이보다 크거나 같을때
    return this._editorListTabs.nativeElement.offsetWidth >= (this._editorListMax.nativeElement.offsetWidth - this._editorListSizeBtn.nativeElement.offsetWidth);
  }

  /**
   * 에디터 결과 width가 max인가
   * @returns {boolean}
   * @private
   */
  private _isEditorResultMaxWidth(): boolean {
    // 현재 생성된 list 의 길이가 최종길이 길이보다 크거나 같을때
    return this._editorResultListTabs.nativeElement.offsetWidth >= this._editorResultListMax.nativeElement.offsetWidth;
  }

  /**
   * 모든 쿼리 저장
   * @private
   */
  private _saveAllQueryEditor() {
    const queryPromise = [];
    this.textList.forEach((item, index) => {
      // 저장할 객체
      const queryEditor: QueryEditor = new QueryEditor();
      queryEditor.editorId = item.editorId;
      queryEditor.name = item.name;
      queryEditor.workbench = CommonConstant.API_CONSTANT.API_URL + 'workbenchs/' + this.workbenchId;
      queryEditor.order = index;
      queryEditor.query = item.query;
      queryPromise.push(this.workbenchService.updateQueryEditor(queryEditor)
        .then(() => {
          // 로컬 스토리지에서 쿼리 삭제
          this.removeLocalStorage(queryEditor.editorId);
        })
        .catch((error) => {
          this.loadingHide();
          if (!isUndefined(error.details)) {
            Alert.error(error.details);
          } else {
            Alert.error(error);
          }
        }));
    });
    Promise.all(queryPromise)
      .then(() => {
      })
      .catch((error) => {
        Alert.error(error);
      });
  }

  /**
   * 결과 수 가 지정되어있는지 판단
   * @returns {boolean}
   */
  private get isEnabledQueryResultNumber(): boolean {
    return !(isUndefined(this.queryResultNumber) || this.queryResultNumber <= 0);
  }

  /**
   * set hive query result ( single query result )
   * @param data
   */
  private setHiveQueryResult(data: any) {

    const currentTabs: ResultTab[] = this._getResultTabsByEditorId(this.executeEditorId);
    const resultTab: ResultTab = this._getResultTab(this.runningResultTabId);
    resultTab.data = data;
    resultTab.editorId = this.executeEditorId;

    if (data.queryResultStatus === 'FAIL') {
      resultTab.name = this._genResultTabName(this.runningQueryEditor.name, 'ERROR', currentTabs.length);
      resultTab.output = 'text';
      resultTab.message = data.message;

      if (this.hiveLogCanceling) {
        Alert.success(this.translateService.instant('msg.bench.alert.log.cancel.success'));
        this.loadingBar.hide();
        this.afterCancelQuery(true);
        return;
      }
    } else {
      resultTab.name = this._genResultTabName(this.runningQueryEditor.name, 'RESULT', currentTabs.length);
      resultTab.output = 'grid';
      resultTab.message = isNullOrUndefined( data.message ) ? '' : data.message;
    }

    // 에디터 결과 슬라이드 버튼 계산
    this._calculateEditorResultSlideBtn();

    // 그리드 표시
    if (this._isEqualRunningVisibleTab()) {
      this.drawGridData();
    }

  } // function - setHiveQueryResult

  /**
   * set query result ( multiple query result )
   * @param {any[]} data
   */
  private setQueryResult(data: any[]) {

    data.forEach((item: any, idx: number) => {

      let selectedYn = false;
      if (0 === idx) {
        selectedYn = true;
        // this.resultTime = item.startDateTime - item.finishDateTime;
      } else {
        selectedYn = false;
      }

      const dataTab: ResultTab = new ResultTab(this.executeEditorId);
      dataTab.data = item;
      dataTab.selected = selectedYn;
      dataTab.showLog = false;

      if (item.queryResultStatus === 'FAIL') {
        dataTab.name = this._genResultTabName(this.runningQueryEditor.name, 'ERROR', (idx + 1));
        dataTab.output = 'text';
        dataTab.message = item.message;
      } else {
        dataTab.name = this._genResultTabName(this.runningQueryEditor.name, 'RESULT', (idx + 1));
        dataTab.output = 'grid';
        dataTab.message = isNullOrUndefined( item.message ) ? '' : item.message;
      }

      (dataTab.selected) && (this.runningResultTabId = dataTab.id);
      this._appendResultTab(dataTab);
    });

    // 에디터 결과 슬라이드 버튼 계산
    this._calculateEditorResultSlideBtn();

    // 그리드 표시
    if (this._isEqualRunningVisibleTab()) {
      this.drawGridData();
    }

    this.loadingBar.hide();

  } // function - setQueryResult

  // 선택된 탭의 텍스트 변경
  private setSelectedTabText(text: string): void {

    // 에디터 텍스트 넣기
    this.editor.setText(text);

    // 에디터 텍스트를 가져와서 로컬 변수에 저장
    this.text = this.getSelectedTabText();

    this.editor.editorFocus();
  }

  // 선택되어 있는 탭안에 전체 텍스트 가져오기
  private getSelectedTabText(): string {

    // 에디터 포커싱
    // this.getEditor().focus();
    // 에디터 텍스트 반환
    return this.editor.value;
  }

  // 선택되어 있는 탭안에 선택한 텍스트 가져오기
  private getSelectedSqlTabText(): string {
    // 에디터 포커싱
    // this.getEditor().focus();
    // 에디터 텍스트 반환
    return this.editor.getSelection();
  }

  // 쿼리 에디터 Tab 값을 세팅한다.
  private readQuery(workBench: Workbench, queryEditors: any[]) {
    if (queryEditors.length === 0) {
      this.createNewEditor('', true, false);
    } else {
      const editors = queryEditors.sort((a, b) => {
        return a.order - b.order;
      });
      // 값 읽고 세팅 하기.
      for (let idx1: number = 0; idx1 < editors.length; idx1 = idx1 + 1) {
        // 로컬 스토리지에 저장된 쿼리가 있다면
        const localQuery = this.getLocalStorageQuery(editors[idx1].id);
        this.textList.push({
          name: editors[idx1].name,
          query: localQuery ? localQuery : editors[idx1].query,
          selected: false,
          editorId: editors[idx1].id,
          editorMode: false
        });

        const generalConnection: any = this.getLocalStorageGeneral();
        if (generalConnection !== null) {
          if (!isUndefined(generalConnection.tabId)) {
            this.selectedTabNum = generalConnection.tabId;
          }
          if (!isUndefined(generalConnection.schema)) {
            this.workbench.dataConnection.database = generalConnection.schema;
          }
        }
        this.tabChangeHandler(this.selectedTabNum, false)
      }

      // 슬라이드 아이콘 show hide
      this._calculateEditorSlideBtn();
    }
  }

  /**
   * Hive result toggle button event
   * @param {boolean} showLog
   */
  public toggleHiveLog(showLog: boolean) {

    const currentTab: ResultTab = this._getCurrentResultTab();

    // 로그 표시 설정
    currentTab.showLog = showLog;
    this.safelyDetectChanges();

    // 그리드 표시
    (currentTab.showLog) || (this.drawGridData());
  } // function - toggleHiveLog

  /**
   * 워크벤치 웹 소켓 생성
   * @param {Function} callback
   */
  private createWebSocket(callback?: Function): void {
    this.workbench = this.workbenchTemp;
    this.websocketId = CommonConstant.websocketId;
    try {
      console.info('this.websocketId', this.websocketId);
      const headers: any = {
        'X-AUTH-TOKEN': this.cookieService.get(CookieConstant.KEY.LOGIN_TOKEN)
      };
      // 메세지 수신
      (this._subscription) && (CommonConstant.stomp.unsubscribe(this._subscription));     // Socket 응답 해제
      this._subscription
        = CommonConstant.stomp.subscribe('/user/queue/workbench/' + this.workbenchId, (data) => {

        ( this.isWaterfallProc && !this.hiveLogCanceling ) && (this.loadingBar.hide());

        const resultTabInfo: ResultTab = this._getResultTab(this.runningResultTabId);
        if (1 === this._getResultTabsByEditorId(this.executeEditorId).length) {
          resultTabInfo.selected = true;
        }

        // console.info('>>>>>> socket data', data);

        // console.info('>>>>>> %s - command : %s', this.runningResultTabId, data.command);

        if (this.isWaterfallProc && !isNullOrUndefined(data.queryIndex)) {

          // log 데이터 그리는 부분, done 인 부분으로 분리
          if ('LOG' === data.command && data.log.length != 0) {

            if (resultTabInfo) {
              resultTabInfo.showLog = true;
              resultTabInfo.log = resultTabInfo.log.concat(data.log);
            }

            resultTabInfo.name = 'Loading..';

            this.safelyDetectChanges();
            // log data 가 있을경우 scroll 이동
            const $logContainer = $('#workbenchHiveLogText');
            if (this._isEqualRunningVisibleTab() && '' !== $logContainer.text()) {
              let textAreaHeight = $logContainer.height();
              let lineBreakLength = $logContainer.find('br').length;
              let offsetTop = textAreaHeight * (Math.ceil(lineBreakLength / 8));

              $logContainer.scrollTop(offsetTop);
            }

          } else if ('DONE' === data.command) {

            // 로그 결과가 미리 떨어지는 경우 대비
            const timer = setInterval(() => {
              const runningTab: ResultTab = this._getResultTab(this.runningResultTabId);
              if (runningTab && runningTab.data) {
                console.info('>>>>>> %s - clear timer', this.runningResultTabId);
                clearInterval(timer);
                this._hiveQueryDone();
              }
            }, 500);

          } // end if - command log, done

        }

        if (data['connected'] === true) {
          console.info('connected');
          this.databaseParam = {
            dataconnection: this.workbenchTemp.dataConnection,
            workbenchId: this.workbenchId,
            webSocketId: CommonConstant.websocketId
          };
        }

        (callback) && (callback.call(this));
      }, headers);
      // 메세지 발신
      const params = {
        username: this.webSocketLoginId,
        password: this.webSocketLoginPw
      };
      CommonConstant.stomp.send('/message/workbench/' + this.workbenchId + '/dataconnections/' + this.workbenchTemp.dataConnection.id + '/connect', params, headers);
    } catch (e) {
      console.info(e);
    }

  } // function - createWebSocket

  /**
   * hive query execute done
   * @return {boolean}
   * @private
   */
  private _hiveQueryDone() {

    if (this.isCancelQuery) {
      this.hiveLogFinish();
      return false;
    }

    // 선택된 탭이 로그가 그려지고 있을경우 그리드 전환
    if (this._isEqualRunningVisibleTab()) {
      const resultTab: ResultTab = this._getResultTab(this.runningResultTabId);
      resultTab.selected = true;
      resultTab.showLog = false;
      this.drawGridData();
    }

    // 마지막 쿼리가 아닐경우 다음 쿼리 호출
    if (!isNullOrUndefined(this.runningQueryArr[this.runningQueryDoneIndex + 1])) {
      this.runningQueryDoneIndex++;
      this.hiveNextQueryExecute();
    } else {
      // finish
      this.hiveLogFinish();
    }

  } // function - _hiveQueryDone

  /**
   * hive connection 다음 쿼리 호출
   * execute
   */
  public hiveNextQueryExecute() {

    // console.info('>>>>>> %s - log next execute', this.runningResultTabId);

    let nextIndex = this.runningQueryDoneIndex;

    // 로그와 데이터 response 통신 부분이 끝나는 시점이 정해져 있지 않기 때문에 강제로 탭을 추가
    // 이전 타이틀 변경 tab title 기존 으로 변경
    const currentResultTab: ResultTab = this._getResultTab(this.runningResultTabId);
    currentResultTab.name = this._genResultTabName(this.runningQueryEditor.name, 'RESULT', nextIndex);

    // 다음 탭 쿼리 호출
    this.runningQueryEditor.query = this.runningQueryArr[nextIndex];
    this.runSingleQueryWithInvalidQuery(this.runningQueryEditor, this.tempEditorSelectedTabNum, nextIndex);

  } // function - hiveNextQueryExecute

  /**
   * hive log DONE 종료
   * @return {boolean}
   */
  public hiveLogFinish() {

    // console.info('>>>>>> %s - log finish', this.runningResultTabId);

    const currentTabs: ResultTab[] = this._getResultTabsByEditorId(this.executeEditorId);
    // 로그 취소된 경우 취소된 결과 탭을 선택 표시
    // currentTabs.forEach(item => {
    //   item.selected = (item.id === this.runningResultTabId);
    // });

    this.isHiveQueryExecute = false;

    // 처음 쿼리를 취소한 경우
    if (currentTabs.length == 0) {
      return false;
    }

    // finish
    // 시점때문에 변경 안된 tab title 기존 으로 변경
    currentTabs.forEach((item: ResultTab, idx: number) => {
      item.name = this._genResultTabName(this.runningQueryEditor.name, 'RESULT', (idx + 1));
      // item.order = idx + 1;
    });

    this.runningQueryDoneIndex = -1;

    // 탭 닫힘 표시
    this.isHiveLog = false;
    this.safelyDetectChanges();
  } // function - hiveLogFinish

  /**
   * 에디터 컴포넌트 래핑 엘리먼트 높이 값 반환
   * @return {number}
   */
  private getEditorComponentElementHeight() {

    // // 에디터 컴포넌트를 감싼 엘리먼트
    const editorWrapElement: Element = this.element.nativeElement
      .querySelector('.ddp-wrap-editor');
    //   .querySelector('.ace_editor');
    //
    // // 에디터 높이
    const editorHeight: number = editorWrapElement.clientHeight;

    // 전체 화면일 경우 계산
    if (this.isQueryEditorFull) {
      const editorFullElement: Element = this.element.nativeElement
        .querySelector('.ddp-ui-query');
      const editorTabElement: Element = this.element.nativeElement
        .querySelector('.ddp-wrap-tabs-edit');

      return editorFullElement.clientHeight - editorTabElement.clientHeight;

    }

    // // 반환
    // return editorHeight;
    return editorHeight;
  } // function - getEditorComponentElementHeight

  /**
   * check key code is escape
   * @param {Event} event
   * @return {boolean}
   */
  private isKeyCodeEscape(event: Event): boolean {
    return event['keyCode'] === 27;
  } // function - isKeyCodeEscape

  // 에디터 컴포넌트 객체 가져오기
  // private getEditor(): CodemirrorComponent {
  //
  //   // 에디터 컴포넌트 객체 반환
  //   return this.editor;
  // }

  /**
   * 그리드 표시
   * @return {boolean}
   */
  private drawGridData() {

    // console.info('>>>>>> %s - drawGridData', this.runningResultTabId);

    this.safelyDetectChanges();

    // const data: any = this._totalResultTabList[idx].data;
    const currentTab: ResultTab = this._getCurrentResultTab();

    if (isNullOrUndefined(currentTab)) {
      return;
    }
    const data: any = currentTab.data;
    const headers: header[] = [];
    // data fields가 없다면 return
    if (!data || !data.fields) {
      // hive 일 경우 log 데이터 확인 필요
      if (this.isWaterfallProc) {

        currentTab.showLog = false;   // 로그 숨김

        // hive 일 경우  field 한 번더 체크
        if (!data || !data.fields) {
          $('.myGrid').html('<div class="ddp-text-result ddp-nodata">' + this.translateService.instant('msg.storage.ui.no.data') + '</div>');
        }
        this.safelyDetectChanges();
        return false;
      }
      this.gridComponent.noShowData();
      $('.myGrid').html('<div class="ddp-text-result ddp-nodata">' + this.translateService.instant('msg.storage.ui.no.data') + '</div>');
      return false;
    }

    for (let index: number = 0; index < data.fields.length; index = index + 1) {
      const temp = data.fields[index].name;
      const columnCnt = temp.length;
      const columnWidth = (7 > columnCnt) ? 80 : (columnCnt * 13.5);
      headers.push(new SlickGridHeader()
        .Id(temp)
        .Name('<span style="padding-left:20px;"><em class="' + this.getFieldTypeIconClass(data.fields[index].type) + '"></em>' + temp + '</span>')
        .Field(temp)
        .Behavior('select')
        .CssClass('cell-selection')
        .Width(columnWidth)
        .CannotTriggerInsert(true)
        .Resizable(true)
        .Unselectable(true)
        .Sortable(true)
        .ColumnType(data.fields[index].logicalType)
        .build()
      );
    }

    const rows: any[] = [];

    for (let idx1: number = 0; idx1 < data.data.length; idx1 = idx1 + 1) {
      const row = {};
      for (let idx2: number = 0; idx2 < data.fields.length; idx2 = idx2 + 1) {
        const temp = data.fields[idx2].name;
        if (data.fields[idx2].logicalType === 'INTEGER') {
          try {
            row[temp] = Number(data.data[idx1][temp]);
          } catch (e) {
            row[temp] = 0;
          }
        } else {
          row[temp] = data.data[idx1][temp];
        }
      }
      rows.push(row);
    }

    // 헤더 필수
    // 로우 데이터 필수
    // 그리드 옵션은 선택
    if (this.gridComponent) {
      this.gridComponent.create(headers, rows, new GridOption()
        .SyncColumnCellResize(true)
        .MultiColumnSort(true)
        .RowHeight(32)
        .CellExternalCopyManagerActivate(true)
        .DualSelectionActivate(true)
        .EnableSeqSort(true)
        .build()
      );
    }

    if (this.searchText !== '') {
      this.gridSearchClear();
    }

    this.safelyDetectChanges();
  } // function - drawGridData

  /**
   * activate editing editor tab name
   */
  public tabLayerModify() {

    if (this.isHiveQueryExecute) {
      Alert.warning(this.translateService.instant('msg.bench.ui.query.run'));
      return;
    }

    this.textList[this.selectedTabNum]['editorMode'] = true;
    this.textList[this.selectedTabNum]['name'] = this.textList[this.selectedTabNum].name;
    this.tabLayer = false;
    this.safelyDetectChanges();

    $(this._editorListTabs.nativeElement).find('input:visible').trigger('focus');
  } // function - tabLayerModify

  /**
   * delete editing editor tab
   */
  public tabLayerDelete() {

    if (this.isHiveQueryExecute) {
      Alert.warning(this.translateService.instant('msg.bench.ui.query.run'));
      return;
    }

    this.closeEditorTab(this.selectedTabNum);
    this.tabLayer = false;
  } // function - tabLayerDelete

  /**
   * 쿼리 cancel - Not Hive
   */
  public cancelRunningQuery(useLog: boolean = false) {
    this.isRunningCancel = true;
    if (useLog) {
      this.hiveLogCanceling = true;
      this.safelyDetectChanges();

      // query cancel 호출
      this.loadingBar.show();
    } else {
      if( this.intervalDownload ) {
        this.loadingBar.hide();
        clearInterval(this.intervalDownload);
      }
    }
    const params = { query: '', webSocketId: this.websocketId };
    this.workbenchService.setQueryRunCancel(this.selectedEditorId, params).then();

    /*
          .then(() => {

            if (useLog) {
              Alert.success(this.translateService.instant('msg.bench.alert.log.cancel.success'));
              this.loadingBar.hide();

              this.afterCancelQuery(true);
            }

          })
          .catch(() => {

            if (useLog) {
              Alert.error(this.translateService.instant('msg.bench.alert.log.cancel.error'));
              this.loadingBar.hide();

              this.afterCancelQuery(false);
            }

          });
    */
  } // function - cancelRunningQuery

  /**
   * after cancel query
   * @param {boolean} isSuccess
   */
  public afterCancelQuery(isSuccess: boolean) {

    console.info('>>>>>> %s - afterCancel', this.runningResultTabId);

    this.hiveLogCanceling = false;

    this.isCancelQuery = true;
    this.isHiveQueryExecute = false;
    this.isRunningCancel = false;

    const runningResultTab: ResultTab = this._getResultTab(this.runningResultTabId);

    runningResultTab.showLog = true;
    if (isSuccess) {
      runningResultTab.log = runningResultTab.log.concat(this.translateService.instant('msg.bench.alert.log.cancel.success'));
      (isNullOrUndefined(runningResultTab.data)) && (runningResultTab.data = {});

      // 시점상 탭 그려지기 이전에 취소한 경우 결과 탭을 생성
      if (isUndefined(runningResultTab)) {
        const resultTabs: ResultTab[] = this._getResultTabsByEditorId(this.executeEditorId);
        const newTab: ResultTab = this._appendResultTabByName(
          this.executeEditorId,
          this._genResultTabName(this.runningQueryEditor.name, 'RESULT', resultTabs.length)
        );
        newTab.output = 'grid';
        newTab.selected = true;
        (isNullOrUndefined(newTab.data)) && (newTab.data = {});
      }

    } else {
      runningResultTab.log = runningResultTab.log.concat(this.translateService.instant('msg.bench.alert.log.cancel.error'));
    }

    this.isHiveLog = false;
    this.safelyDetectChanges();
    /*
        if( this.hiveLogCanceling ) {
          this.hiveLogCanceling = false;

          const runningResultTab: ResultTab = this._getResultTab(this.runningResultTabId);

          runningResultTab.showLog = true;
          if (isSuccess) {
            this.isCancelQuery = true;
            this.isHiveQueryExecute = false;
            this.isHiveLog = false;
            runningResultTab.log = runningResultTab.log.concat(this.translateService.instant('msg.bench.alert.log.cancel.success'));
            ( isNullOrUndefined( runningResultTab.data ) ) && ( runningResultTab.data = {} );

            // 시점상 탭 그려지기 이전에 취소한 경우 결과 탭을 생성
            if (isUndefined(runningResultTab)) {
              const resultTabs: ResultTab[] = this._getResultTabsByEditorId(this.executeEditorId);
              const newTab: ResultTab = this._appendResultTabByName(
                this.executeEditorId,
                this._genResultTabName(this.runningQueryEditor.name, 'RESULT', resultTabs.length)
              );
              newTab.output = 'grid';
              newTab.selected = true;
              ( isNullOrUndefined( newTab.data ) ) && ( newTab.data = {} );
            }

          } else {
            runningResultTab.log = runningResultTab.log.concat(this.translateService.instant('msg.bench.alert.log.cancel.error'));
          }

          this.safelyDetectChanges();
        }
    */

  } // function - afterCancelQuery

  // 뒤로 돌아가기
  public goBack() {
    // unload false
    this.useUnloadConfirm = false;
    const cookieWs = this.cookieService.get(CookieConstant.KEY.CURRENT_WORKSPACE);
    let cookieWorkspace = null;
    if (cookieWs) {
      cookieWorkspace = JSON.parse(cookieWs);
    }
    if (null !== cookieWorkspace) {
      this.router.navigate(['/workspace', cookieWorkspace['workspaceId']]);
    }
  }

  // sql 포맷터
  public setSqlFormatter() {

    // let textAll: string = this.editor.value;
    const textSelected: string = this.editor.getSelection();

    if (textSelected === '') {
      Alert.info(this.translateService.instant('msg.bench.alert.no.selected.query'));
      return;
    }

    let text: string = this.editor.formatter(textSelected, ' ');
    this.editor.replace(text);
    console.info('formatter', text);
    //text = this.replaceAll(text, ';', ';\n\n');

    // textAll = this.replaceAll(textAll, textSelected, text);
    // this.editor.setText(textAll);
  }

  public replaceAll(str, find, replace) {
    return str.replace(new RegExp(this.escapeRegExp(find), 'g'), replace);
  }

  // noinspection JSMethodCanBeStatic
  public escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
  }

  /**
   * 데이터소스 생성
   */
  public createDatasource() {
    const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
    const currentResultTab: ResultTab = currentEditorResultTabs.find(item => item.selected);

    if (0 === currentEditorResultTabs.length) {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    if ('text' === currentResultTab.output) {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    this.connectionService.getDataconnectionDetail(this.workbench.dataConnection.id)
      .then((connection) => {
        const selectedSecurityType = [
          { label: this.translateService.instant('msg.storage.li.connect.always'), value: 'MANUAL' },
          { label: this.translateService.instant('msg.storage.li.connect.account'), value: 'USERINFO' },
          { label: this.translateService.instant('msg.storage.li.connect.id'), value: 'DIALOG' }
        ].find(type => type.value === this.workbench.dataConnection.authenticationType) || {
          label: this.translateService.instant('msg.storage.li.connect.always'),
          value: 'MANUAL'
        };
        this.mainViewShow = false;
        this.mode = 'db-configure-schema';
        this.setDatasource = {
          connectionData: {
            connectionId: this.workbench.dataConnection.id,
            hostname: this.workbench.dataConnection.hostname,
            port: this.workbench.dataConnection.port,
            url: this.workbench.dataConnection.url,
            username: selectedSecurityType.value === 'DIALOG' ? this.webSocketLoginId : connection.username,
            password: selectedSecurityType.value === 'DIALOG' ? this.webSocketLoginPw : connection.password,
            selectedDbType: this.getEnabledConnectionTypes(true)
              .find(type => type.value === this.workbench.dataConnection.implementor.toString()),
            selectedSecurityType: selectedSecurityType,
            selectedIngestionType: {
              label: this.translateService.instant('msg.storage.ui.list.ingested.data'),
              value: ConnectionType.ENGINE
            },
            isUsedConnectionPreset: true
          },
          databaseData: {
            selectedType: 'QUERY',
            selectedDatabaseQuery: this.workbench.dataConnection.database,
            queryText: currentResultTab.data['runQuery'],
            queryDetailData: {
              fields: currentResultTab.data['fields'],
              data: currentResultTab.data['data']
            }
          },
          workbenchFl: true
        };

        // 로딩 hide
        this.loadingHide();
      })
      .catch((error) => {
        // 로딩 hide
        this.loadingHide();
        if (!isUndefined(error.details)) {
          Alert.error(error.details);
        } else {
          Alert.error(error);
        }
      });

  } // function - createDatasource

  /**
   * 데이터 소스 생성 후 처리
   */
  public createDatasourceComplete() {
    this.mainViewShow = true;
    this.mode = '';
  } // function - createDatasourceComplete

  /**
   * 임시 데이터 소스 생성 전처리
   */
  public createDatasourceTemporary() {

    const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
    const currentResultTab: ResultTab = currentEditorResultTabs.find(item => item.selected);

    if (0 === currentEditorResultTabs.length) {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    if (currentResultTab.output === 'text') {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    this.loadingShow();
    this.connectionService.getDataconnectionDetail(this.workbench.dataConnection.id)
      .then((connection) => {
        // 로딩 hide
        this.loadingHide();
        this.createDatasourceTemporaryDetail(connection.username, connection.password, connection);
      })
      .catch((error) => {
        // 로딩 hide
        this.loadingHide();
        Alert.error(error);
      });
  } // function - createDatasourceTemporary

  /**
   * 임시 데이터 소스 생성
   * @param {string} id
   * @param {string} pw
   * @param connection
   */
  public createDatasourceTemporaryDetail(id: string, pw: string, connection: any) {

    const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
    const currentResultTab: ResultTab = currentEditorResultTabs.find(item => item.selected);

    if (0 === currentEditorResultTabs.length) {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    if (currentResultTab.output === 'text') {
      Alert.info(this.translateService.instant('msg.bench.alert.no.result'));
      return;
    }

    // 로딩 show
    this.loadingShow();

    // TODO JDBC
    // params
    // TODO published true 나중에 삭제할것
    this.pageEngineName = 'bulk_ingestion_' + Date.now();
    const param = {
      dsType: 'MASTER',
      connType: 'LINK',
      srcType: 'JDBC',
      granularity: 'DAY',
      segGranularity: 'MONTH',
      engineName: this.pageEngineName,
      name: this.pageEngineName,
      description: ''
    };


    // fields param
    let column = [];
    // 타임스탬프로 지정된 컬럼이 없을 경우
    const field = {
      name: 'current_datetime',
      type: 'TIMESTAMP',
      role: 'TIMESTAMP',
      format: 'yyyy-MM-dd HH:mm:ss',
      aggrType: 'NONE',
      biType: 'TIMESTAMP',
      logicalType: 'STRING'
    };
    column.push(field);

    column = column.concat(currentResultTab.data['fields']);
    let seq = 0;
    column.forEach((item) => {
      item['seq'] = seq;
      seq += 1;

      // ingestion rule 이 존재시
      if (item['ingestionRule']) {
        const type = item.ingestionRule.type;

        switch (type) {
          case 'default':
            delete item['ingestionRule'];
            break;
          case 'discard':
            delete item.ingestionRule.value;
        }
      }

      // time stamp field
      // if (item['timestampFl'] === true) {
      //   item['role'] = 'TIMESTAMP';
      // }

      // 필요없는 변수 삭제
      delete item['checked'];
      delete item['timestampFl'];
      delete item['deleteFl'];
    });
    param['fields'] = column;

    const selectedSecurityType = [
      { label: this.translateService.instant('msg.storage.li.connect.always'), value: 'MANUAL' },
      { label: this.translateService.instant('msg.storage.li.connect.account'), value: 'USERINFO' },
      { label: this.translateService.instant('msg.storage.li.connect.id'), value: 'DIALOG' }
    ].find(type => type.value === connection.authenticationType) || {
      label: this.translateService.instant('msg.storage.li.connect.always'),
      value: 'MANUAL'
    };

    // ingestion param
    const connInfo: Dataconnection = this.workbench.dataConnection;
    param['ingestion'] = {
      type: 'link',
      connection: {
        implementor: connInfo.implementor,
        type: connInfo.type,
        hostname: connInfo.hostname,
        port: connInfo.port,
        url: connInfo.url,
        database: connInfo.database,
        authenticationType: this.authenticationType,
        catalog: connInfo.catalog,
        sid: connInfo.sid,
        connectUrl: this._isUrlType() ? connInfo.url : ('jdbc:' + connInfo.implementor.toString().toLowerCase() + '://' + connInfo.hostname + ':' + connInfo.port + '/' + connInfo.database),
      },
      database: connInfo.database,
      dataType: 'QUERY',
      query: currentResultTab.data['runQuery']
    };

    if (selectedSecurityType.value === 'DIALOG') {
      param['ingestion'].connectionUsername = this.webSocketLoginId;
      param['ingestion'].connectionPassword = this.webSocketLoginPw;
    } else if (selectedSecurityType.value === 'MANUAL') {
      param['ingestion'].connection.username = id;
      param['ingestion'].connection.password = pw;
    }


    this.loadingShow();
    this.datasourceService.createDatasourceTemporary(param).then((tempDsInfo) => {
      this.setPageWidget(tempDsInfo);
      setTimeout(() => this.loadingHide(), 500);
    }).catch((error) => {
      this.loadingHide();
      // 로딩 hide
      if (!isUndefined(error.message)) {
        Alert.error(error.message);
      }
    });
  }

  /**
   * 차트 미리보기를 위한 페이지 위젯 정보 구성
   * @param {Object} temporary
   */
  public setPageWidget(temporary: Object) {
    const tempWidget = new PageWidget();

    // 데이터소스 구성
    const boardDataSource: BoardDataSource = new BoardDataSource();
    {
      boardDataSource.id = temporary['dataSourceId'];
      boardDataSource.type = 'default';
      boardDataSource.name = temporary['name']; // this.pageEngineName;
      boardDataSource.engineName = temporary['name']; // this.pageEngineName;
      boardDataSource.connType = 'LINK';
      boardDataSource.temporary = true;
      // boardDataSource.fields = this._totalResultTabList[this.selectedGridTabNum]['data']['fields'];
      tempWidget.configuration.dataSource = boardDataSource;
    }

    // 가상 대시보드 정보 구성
    {
      const dashboard: Dashboard = new Dashboard();
      dashboard.name = 'temporary';
      dashboard.configuration = new BoardConfiguration();
      dashboard.configuration.dataSource = boardDataSource;   // 대시보드 데이터소스 설정

      // 워크북 설정
      const workbook: Workbook = new Workbook();
      workbook.name = '';
      dashboard.workBook = workbook;

      // 대시보드 필드 정보 설정
      const currentEditorResultTabs: ResultTab[] = this._getCurrentEditorResultTabs();
      const currentResultTab: ResultTab = currentEditorResultTabs.find(item => item.selected);
      let fields = currentResultTab.data['fields'];

      const currentDateTimeField: Field = new Field();
      currentDateTimeField.name = 'current_datetime';
      currentDateTimeField.biType = BIType.TIMESTAMP;
      currentDateTimeField.logicalType = LogicalType.TIMESTAMP;
      currentDateTimeField.dataSource = boardDataSource.engineName;
      // fields = [currentDateTimeField].concat( fields );
      fields.forEach(item => item.dataSource = boardDataSource.engineName);
      dashboard.configuration.fields = fields;

      // 대시보드 필터 구성
      // const filter: TimeAllFilter = FilterUtil.getTimeAllFilter(currentDateTimeField, 'general');
      // dashboard.configuration.filters = [filter];
      dashboard.configuration.filters = [];

      // 가상 대시보드 데이터소스 설정
      {
        const datasource: Datasource = new Datasource();
        datasource.id = boardDataSource.id;
        datasource.fields = fields;
        datasource.name = temporary['name'];
        datasource.engineName = temporary['name'];
        dashboard.dataSources = [datasource];
      }

      tempWidget.dashBoard = dashboard;
    }

    this.selectedPageWidget = tempWidget;
    this.mainViewShow = false;
    this.isShowPage = true;
  } // function - setPageWidget

  /**
   * Workbench 이름 수정
   */
  public onWorkbenchNameEdit($event) {
    if (this.isChangeAuthUser) {
      $event.stopPropagation();
      this.isWorkbenchNameEditMode = !this.isWorkbenchNameEditMode;

      if (this.isWorkbenchDescEditMode) {
        this.isWorkbenchDescEditMode = false;
      }

      if (this.workbenchDesc !== this.workbenchTemp.description) {
        this.workbenchDesc = this.workbenchTemp.description;
      }

      this.changeDetect.detectChanges();
      this.wbName.nativeElement.focus();
    }
  }

  /**
   * Workbench 설명 수정
   */
  public onWorkbenchDescEdit($event) {
    if (this.isChangeAuthUser) {
      $event.stopPropagation();
      this.isWorkbenchDescEditMode = !this.isWorkbenchDescEditMode;

      if (this.isWorkbenchNameEditMode) {
        this.isWorkbenchNameEditMode = false;
      }

      if (this.workbenchName !== this.workbenchTemp.name) {
        this.workbenchName = this.workbenchTemp.name;
      }

      this.changeDetect.detectChanges();
      this.wbDesc.nativeElement.focus();
    }
  }

  /**
   * Set workbench name (실제 워크벤치 이름을 this.workbenchName에 넣는다)
   */
  public setWorkbenchName() {
    this.isWorkbenchNameEditMode = false;
    if (this.workbenchTemp.name !== this.workbenchName) {
      this.workbenchName = this.workbenchTemp.name;
    }
  }

  /**
   * Set workbench desc (실제 워크벤치 설명을 this.workbenchDesc 넣는다)
   */
  public setWorkbenchDesc() {
    this.isWorkbenchDescEditMode = false;
    if (this.workbenchTemp.description !== this.workbenchDesc) {
      this.workbenchDesc = this.workbenchTemp.description;
    }
  }

  // *****************************************************************
  // 스키마 브루어져 관련
  // *****************************************************************

  /**
   * 스키마 브라우져 창 열기
   */
  public setSchemaBrowser(): void {
    const param = {
      workbench: this.workbench,
      workbenchId: this.workbenchId,
      websocketId: this.websocketId,
      textList: this.textList
    };
    this.schemaBrowserComponent.init(param);
  }

  public setTableDataEvent($event) {
    let tableTemp: any = {};
    $event.forEach(item => {
      tableTemp[item.name] = []
    });

    this.editor.setOptions(tableTemp);

    //H2, HIVE, ORACLE, TIBERO, MYSQL, MSSQL, PRESTO, FILE, POSTGRESQL, GENERAL;
    if (this.mimeType == 'HIVE' || this.mimeType == 'PRESTO' || this.mimeType == 'GENERAL') {
      this.editor.setModeOptions('text/x-hive');
    } else {
      this.editor.setModeOptions('text/x-mysql');
    }
  }

  // *****************************************************************
  // Result Tab 관련
  // *****************************************************************

  /**
   * generate result tab name
   * @param {string} prefix
   * @param {"result" | "error"} type
   * @param {number} idx
   * @private
   */
  private _genResultTabName(prefix: string, type: 'RESULT' | 'ERROR', idx: number) {
    return prefix + ' - ' + this.translateService.instant('RESULT' === type ? 'msg.bench.ui.rslt' : 'msg.comm.ui.error') + idx;
  } // function - _genResultTabName

  /**
   * return result tab list in current selected editor
   * @return {ResultTab[]}
   * @private
   */
  private _getCurrentEditorResultTabs(): ResultTab[] {
    return this.totalResultTabList.filter(item => item.editorId === this.selectedEditorId);
  } // function - _getCurrentEditorResultTabs

  /**
   * return current visible result tab
   * @return {ResultTab}
   * @private
   */
  private _getCurrentResultTab(): ResultTab {
    return this.totalResultTabList.find(item => item.editorId === this.selectedEditorId && item.selected);
  } // function - _getCurrentResultTab

  /**
   * return result tab list in editor id
   * @param {string} editorId
   * @return {ResultTab[]}
   * @private
   */
  private _getResultTabsByEditorId(editorId: string): ResultTab[] {
    return this.totalResultTabList.filter(item => item.editorId === editorId);
  } // function - _getResultTabsByEditorId

  /**
   * return result tab by tabId
   * @param {string} tabId
   * @return {ResultTab}
   * @private
   */
  private _getResultTab(tabId: string): ResultTab {
    return this.totalResultTabList.find(item => item.id === tabId);
  } // function - _getResultTab

  /**
   * remove result tab
   * @return {ResultTab[]}
   * @private
   */
  private _removeResultTab(resultTabId: string): ResultTab[] {
    const rmIdx: number = this.totalResultTabList.findIndex(item => item.id === resultTabId);
    if (-1 < rmIdx) {
      this.totalResultTabList.splice(rmIdx, 1);
    }
    return this.totalResultTabList;
  } // function - _removeResultTab

  /**
   * clear result tab list in current selected editor
   * @return {ResultTab[]}
   * @private
   */
  private _clearCurrentEditorResultTabs(): ResultTab[] {
    this.totalResultTabList = this.totalResultTabList.filter(item => item.editorId !== this.executeEditorId);
    return this.totalResultTabList;
  } // function - _clearCurrentEditorResultTabs

  /**
   * append result tab
   * @param {string} editorId
   * @param {string} name
   * @return {ResultTab}
   * @private
   */
  private _appendResultTabByName(editorId: string, name: string): ResultTab {
    const tabInfo = new ResultTab(editorId);
    tabInfo.name = 'Loading..';
    this._appendResultTab(tabInfo);
    return tabInfo;
  } // function - _appendResultTabByName

  /**
   * append result tab
   * @param {ResultTab} resultTab
   * @private
   */
  private _appendResultTab(resultTab: ResultTab) {
    this.totalResultTabList.push(resultTab);
    // this.totalResultTabList
    //   .filter( item => item.editorId === resultTab.editorId )
    //   .forEach( ( item:ResultTab, idx:number ) => {
    //     item.order = idx + 1;
    //   });
  } // function - _appendResultTab

  /**
   * check running tab equal visible tab
   * @return {boolean}
   * @private
   */
  private _isEqualRunningVisibleTab(): boolean {
    const runningTab: ResultTab = this._getResultTab(this.runningResultTabId);
    const visibleTab: ResultTab = this._getCurrentResultTab();
    return visibleTab && runningTab && runningTab.id === visibleTab.id;
  } // function - _isEqualRunningVisibleTab

}

// 리스트 슬라이드아이콘 관리용 객체
class EditorList {
  // list index
  public index: number = 0;
  // btn show flag
  public showBtnFl: boolean = false;
  // list
  public list: any = [];
}

/**
 * Result Tab Object
 */
class ResultTab {
  public editorId: string;      // target editor id
  public id: string;            // tag id
  public name: string;
  public message: string;
  public output: string;        // grid or text
  public selected: boolean;
  public showLog: boolean;
  public log: string[];
  public data?: any;           // Result

  constructor(editorId: string) {
    this.id = CommonUtil.getUUID();
    this.editorId = editorId;
    this.selected = false;
    this.showLog = true;
    this.log = [];
  }
}
